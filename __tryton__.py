# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Taxes for Germany (Cash Basis)',
    'name_de_DE': 'Buchhaltung Gültigkeitsdauer Steuern für Deutschland'
        ' (Ist-Versteuerung)',
    'version': '2.2.0',
    'author': 'MBSolutions',
    'email': 'info@m9s.biz',
    'website': 'http://www.m9s.biz',
    'description': '''Tax Data for Germany Cash Basis
    Provides data for german cash basis accounting
    - taxes
    - product tax types
    - tax groups
''',
    'description_de_DE': '''Steuerdaten für Deutschland Ist-Versteuerung
    Fügt folgende Daten für Ist-Versteuerung Deutschland hinzu:
    - Steuern
    - Steuertypen für Artikel
    - Steuergruppen
''',
    'depends': [
        'account_timeline',
        'account_timeline_tax_de',
        'account_tax_cash_basis',
    ],
    'xml': [
        'account_timeline_tax_de_cash_basis.xml',
    ],
    'translation': [
        #'locale/de_DE.po',
    ],
}
